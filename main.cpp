/******************************************************************************

Welcome to GDB Online.
  GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
  C#, OCaml, VB, Perl, Swift, Prolog, Javascript, Pascal, COBOL, HTML, CSS, JS
  Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <iostream>
#define PI 3.14159265

class Padre {
public:
    double grados(double grados) {
        return grados * (PI / 180);
    }

    double radianes(double radianes) {
        return radianes * (180 / PI);
    }
};

class Hijo : public Padre {
public:
    double kilometros(double kilometros) {
       if (kilometros >= 0) {
            return kilometros * 0.621371;
        } else {
            std::cout << "No se pueden convertir kilometros negativos." << std::endl;
            return 0;
        }
    }

    double millas(double millas) {
        if (millas >= 0) {
            return millas * 1.60934;
        } else {
            std::cout << "No se pueden convertir millas negativas." << std::endl;
            return 0;
        }
    }
};

int main() {
    Hijo Hijo;
    int opc;
    double valor;

    do {
        std::cout << "Selecciona una operacion:\n";
        std::cout << "1. Convertir grados a radianes\n";
        std::cout << "2. Convertir radianes a grados\n";
        std::cout << "3. Convertir kilometros a millas\n";
        std::cout << "4. Convertir millas a kilometros\n";
        std::cout << "0. Salir\n";
        std::cout << "Ingresa el numero de la operacion: ";
        std::cin >> opc;

        switch (opc) {
            case 1:
                std::cout << "Ingresa los grados: ";
                std::cin >> valor;
                std::cout << valor << " grados = " << Hijo.grados(valor) << " radianes" << std::endl;
                break;
            case 2:
                std::cout << "Ingresa los radianes: ";
                std::cin >> valor;
                std::cout << valor << " radianes = " << Hijo.radianes(valor) << " grados" << std::endl;
                break;
            case 3:
                std::cout << "Ingresa los kilometros: ";
                std::cin >> valor;
                std::cout << valor << " kilometros = " << Hijo.kilometros(valor) << " millas" << std::endl;
                break;
            case 4:
                std::cout << "Ingresa las millas: ";
                std::cin >> valor;
                std::cout << valor << " millas = " << Hijo.millas(valor) << " kilometros" << std::endl;
                break;
            case 0:
                std::cout << "Saliendo del programa" << std::endl;
                break;
            default:
                std::cout << "Opcion invalida. Ingresa un numero valido." << std::endl;
                break;
        }
    } while (opc != 0);

    return 0;
    
    //Explica brevemente en que consiste la herencia en c++ y cuales son sus beneficios en la programacion orientada a objetos:
//La herencia en C++ permite que una clase nueva (clase hija) herede atributos y métodos de una clase existente (clase padre), fomentando la reutilización del código, la creación de jerarquías de clases para una mejor organización, y la capacidad de extender y modificar el comportamiento de las clases base, lo que resulta en un código más flexible y estructurado en la programación orientada a objetos.

}






